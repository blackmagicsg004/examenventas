package registrosVenta;

/**
 *
 * @author Black
 */
public class RegistroV {
       
     
    private int codigo;
    private int litros;
    private int tipo;
   
    
    public RegistroV(){
    this.codigo=0;
    this.litros=0;
    this.tipo=0;
  
    
    
    }
    //Constructor por argumentos
    public RegistroV(int codigo, int gasTipo, int litros, int tipo){
    this.codigo=codigo;
    this.litros=litros;
    this.tipo=tipo;
   
    }
    
    //Copia "OTRO"
    public RegistroV(RegistroV otro){
    this.codigo=otro.codigo;
    this.litros=otro.litros;
    this.tipo=otro.tipo;
    
    }
    //Metodos Set y Get
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getLitros() {
        return litros;
    }

    public void setLitros(int litros) {
        this.litros = litros;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
      //Metodos de comportamiento
    
    public float calcularCantidad(){
    int cantidad=0;
    cantidad=this.litros;
    return cantidad;
    }
    
        public float calcularPrecioLitro(){
    float precioL=0.0f;
    if (this.tipo == 1) {
        precioL=(this.litros*24.50f);
    }
    if (this.tipo == 2) {
        precioL=(this.litros*20.50f);
    }
    return precioL;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularPrecioLitro()*this.calcularCantidad();
    return total;
    }
    
}

