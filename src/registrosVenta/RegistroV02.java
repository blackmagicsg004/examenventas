package registrosVenta;

/**
 *
 * @author Black
 */
public class RegistroV02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        RegistroV RegistroV02 = new RegistroV();
        RegistroV02.setCodigo(89232);
        RegistroV02.setTipo(1);
        RegistroV02.setLitros(4);
       
        
        System.out.println("codigo: "+ RegistroV02.getCodigo());
        System.out.println("tipo de gasolina: "+ RegistroV02.getTipo()+(": premium"));
        System.out.println("cantidad de gasolina: "+ RegistroV02.getLitros()+(" litros"));
        System.out.println("cantidad: "+ RegistroV02.calcularCantidad());
        System.out.println("precio por litro de gasolina: " + RegistroV02.calcularPrecioLitro());  
         System.out.println("total: " + RegistroV02.calcularTotal()); 
    }
    
}
